# Copyright (c) 2017, Julian Thome
#
# Released under the MIT license
# https://opensource.org/licenses/MIT

FROM frolvlad/alpine-glibc
MAINTAINER julian.thome.de@gmail.com

ENV PATH /usr/local/texlive/2017/bin/x86_64-linux:/opt/latexdiff:$PATH

RUN apk --no-cache add -U git bash perl python wget xz; \
	mkdir -p /opt/texlive; \
	cd /opt/texlive; \
	wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl.zip; \
	unzip install-tl.zip; mv install-tl-* install-tl

COPY texlive.profile /opt/texlive/install-tl

COPY latexdiff /opt/latexdiff

RUN cd /opt/texlive/install-tl; \
	./install-tl -portable -profile=texlive.profile; \
	rm -rf /opt/texlive

RUN mkdir /opt/wdir
WORKDIR /wdir
VOLUME ["/wdir"]
CMD ["bash"]
