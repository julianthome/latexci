# latexci

`latexci` is a docker image that can be used to automatically compile and
generate diff PDFs between LaTeX documents which are made available as
artifacts after the execution of the CI/CD pipeline.

## Configuring your GitLab CI/CD workflow

``` yaml
image: julianthome/latexci:latest

pdfbuild:
  stage: build
  script: texliveonfly -c latexmk -a "-pdf -f -synctex=0" main.tex
  artifacts:
    paths:
      - main.pdf
    expire_in: 1 week

pdfdiff:
  stage: build
  script: do_latexdiff_ci.sh main.tex
  artifacts:
    paths:
      - diff.pdf
    expire_in: 1 week
  allow_failure: true
```

For configuring your GitLab CI/CD workflow, you can add the settings above to
your `.gitlab-ci.yml` file. Please note that you can replace `main.tex` in the excerpt above
with you latex root document.

The `pdfbuild` job automatically generates a PDF that is downloadable as
artifact.  

The `pdfdiff` job creates a PDF that highlight the changes made between to
versions of a LaTeX document. There are two ways how you can specify which
version to consider as a baseline: 

1. tag the version (using `git tag`) you would like to diff against using `baseline-diff` as tag name
1. add a file named `.basseline-diff` to the git repository that contains the SHA hash of the git
revision you would like to diff against 

After using one of the strategies above, the `pdfdiff` job will generate a diff
PDF file that is downloadable as artifact.

For convenience, to get direct access to both artifacts, you may want to add the
following badges to your `README.md` file. Please, replace `<repo_url>` with
your repository url.

```
- [Latest Paper Build (PDF)](https://gitlab.com/gitlab-org/<repo_url>/-/jobs/artifacts/master/raw/main.pdf?job=pdfbuild)
- [Latest Paper Diff (PDF)](https://gitlab.com/gitlab-org/<repo_url>/-/jobs/artifacts/master/raw/diff.pdf?job=pdfdiff)
```

## Latexci Docker image

`latexci` is based on alpine linux and contains a medium texlive installation
which should be sufficient for most of the use-cases. However, this
configuration can be easily changed in the `texlive.profile` file. For more
information, please consult the [texlive
manual](https://www.tug.org/texlive/doc/texlive-en/texlive-en.html#tlportable).
In case your LaTeX document relies on packages that are not available, please
use [texliveonfly](https://ctan.org/pkg/texliveonfly?lang=de) for building your
latex documents in your CI/CD chain; `texliveonfly` automatically fetches
packages which are not installed, yet:

```bash
texliveonfly -c latexmk -a "-pdf -f -synctex=0" mydoc.tex
```

### Installation instructions

```bash
# build the docker container
docker build -t latexci .
# start the container
docker run -it latexci
```

### Releases

All the releases are made available on [Docker Hub](https://hub.docker.com/r/julianthome/latexci). You can pull the latest
image by executing the command below.

``` 
docker pull julianthome/latexci:latest 
```
