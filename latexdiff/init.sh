#!/bin/bash
# Copyright (c) 2018, Julian Thome
#
# Released under the MIT license
# https://opensource.org/licenses/MIT

CURDIR=$( dirname "${BASH_SOURCE[0]}" )

TEXLIVEONFLY="texliveonfly -c latexmk -a '-pdf -f -synctex=0'"
