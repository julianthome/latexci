#!/bin/bash
# Copyright (c) 2018, Julian Thome
#
# Released under the MIT license
# https://opensource.org/licenses/MIT

source "$(dirname "${BASH_SOURCE[0]}")/init.sh"

GITREPO="$1"
OLD="$2"
NEW="$3"
MAIN="$4"
# get directory of this script -- used to determine path of exclude.cfg
EXCLUDES="$CURDIR/excludes"
TMPDIR="$(mktemp -d)"

OLDNAME="old"
NEWNAME="new"
DIFFNAME="diff"

EXCLUSION_CFGS=$(find "$EXCLUDES" -mindepth 1 -name "*.cfg" -type f)

[ -z "$MAIN" ] || [ -z "$OLD" ] || [ -z "$NEW" ] || [ -z "$GITREPO" ] && {
    echo "usage: $0 <repo-url> <old-revision> <new-revision> <main-file>"
    echo "<repo-url|path>: the (url of | path to) the git repository with the latex sources"
    echo "<old-revision>: the hash of the old revision"
    echo "<new-revision>: the hash of the new revision"
    echo "<main-file>: the name name of the latex main file (including .tex extension)"
    exit 1
}

function validate() {
    # this is used to ensure that the documents compile and the required packages are
    # installed

    local vdir="$1"

    cd "$vdir" && /bin/bash -c "$TEXLIVEONFLY $MAIN" || {
	echo "could not build document for revision $1"
    	exit 1
    }

    cd "$vdir" && latexmk -C 
}

function getrev () {
    # can be either 'local' or 'remote'
    local type="$1"
    local rev="$2"
    local tdir="$3" 

    [ -z "$rev" ] || [ -z "$tdir" ] && {
        echo "malformed rev hash $rev"
        exit 1
    }
  
    local xcmd=""

    if [ "$type" == "remote" ]; then
        cd "$TMPDIR" && git clone "$GITREPO" "$tdir"
    elif [ "$type" == "local" ]; then
        cd "$TMPDIR" && cp -a "$GITREPO" "$TMPDIR/$tdir"
    else
        echo "type can be only 'local' or 'remote'"
	exit 1
    fi

    # checkout the desired revision only if the directories do not exist yet
    $(cd "$TMPDIR/$tdir"; git checkout "$rev") || {
        echo "could not clone $GITREPO"
        exit 1
    }

    validate "$TMPDIR/$tdir" 
}

echo "trying to create diff in $TMPDIR ..."

if [ -d "$GITREPO" ]; then
# get check remote repo
    echo "create local copies of $GITREPO"
    getrev "local" "$OLD" "$OLDNAME"
    getrev "local" "$NEW" "$NEWNAME"
else
# repo is already on the file system -- just copy it and checkout the right revs
    echo "get remote repos from $GITREPO"
    getrev "remote" "$OLD" "$OLDNAME"
    getrev "remote" "$OLD" "$OLDNAME"
fi

NEWTARGET="$TMPDIR/$NEWNAME/$MAIN"
OLDTARGET="$TMPDIR/$OLDNAME/$MAIN"
DIFFTARGET="$TMPDIR/$NEWNAME/$DIFFNAME.tex"

[ ! -f "$NEWTARGET" ] || [ ! -f "$OLDTARGET" ] && {
    echo "$NEWTARGET or $OLDTARGET do(es) not exist"
    exit 1
}

for cfg in $EXCLUSION_CFGS; do
    latexdiff --flatten  \
        --math-markup=0 --flatten -c "$cfg" \
        "$OLDTARGET" "$NEWTARGET" > "$DIFFTARGET"

    # use options f and nonstopmode to force the script to continue
    cd "$TMPDIR/$NEWNAME" && latexmk -f -interaction=nonstopmode -pdf "$DIFFNAME.tex"

    [ -e "$TMPDIR/$NEWNAME/${DIFFNAME}.pdf" ] && {
        echo "$TMPDIR/$NEWNAME/${DIFFNAME}.pdf successfully generated"
        cp "$TMPDIR/$NEWNAME/${DIFFNAME}.pdf" "/tmp/${DIFFNAME}.pdf"
        echo "Diff available at /tmp/${DIFFNAME}.pdf"
        exit 0
    }
done

echo "no PDF output produced"

exit 1
