#!/bin/bash
# Copyright (c) 2018, Julian Thome
#
# Released under the MIT license
# https://opensource.org/licenses/MIT

source "$(dirname "${BASH_SOURCE[0]}")/init.sh"

$(cd "$CURDIR" && git submodule init)
$(cd "$CURDIR" && git submodule update --recursive --remote)

MAIN="$1"
OLD="$2"
NEW="$3"

GIT_LDIF="$CURDIR/git-latexdiff/git-latexdiff"

[ -z "$MAIN" ] || [ -z "$OLD" ] || [ -z "$NEW" ] && {
    echo "usage: $0 <main-file> <old-revision> <new-revision>"
    echo "<main-file>: the name name of the latex main file (including .tex extension)"
    echo "<old-revision>: the hash of the old revision"
    echo "<new-revision>: the hash of the new revision"
    exit 1
}


echo "$MAIN $OLD $NEW"

$GIT_LDIF --main "$MAIN" --latexmk \
    --latexdiff-flatten -o "diff.pdf" "$OLD" "$NEW"

exit 0
