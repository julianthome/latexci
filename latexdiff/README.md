# Scripts for simplifying the latexdiff generation

1. **do_gitlatexdiff.sh**: A script for generating the diff (PDF) between two
latex document revisions. This script relies on the git-latexdiff tool which is
automatically pulled when running this script. Note, that in many cases (when
the latex document is complex, contains a lot of tables, tikz figures, etc.)
git-latexdiff fails to produce a PDF. In this case, it is better to use
`do_latexdiff` because it allows you to exclude complex figures.
... Usage: `do_gitlatexdiff.sh <main-file> <old-revision> <new-revision>`

2. **do_latexdiff.sh**: A `latexdiff` wrapper for generating the diff (PDF)
between two latex document revisions. As opposed to `git-latexdiff` it allows
you to exclude certain LaTeX environments from the document generation. It
applies all exclusion rules contained in the `excludes` directory unless a PDF
could be successfully generated.
... Usage: `do_latexdiff.sh <repo-url> <old-revision> <new-revision> <main-file>`

3. **do_latexdiff_ci.sh**: A `do_latexdiff` wrapper that can be used to run the
script in CI mode. In order to determine the version to diff against, this
script checks for the git tag `baseline-diff`. If it does not succeed, it will
look for a hidden file `.baseline-diff` in the repository which should contain
the hash of the revision to diff against.
... Usage: `do_latexdiff_ci.sh <main-file>`

