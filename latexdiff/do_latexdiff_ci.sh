#!/bin/bash
# Copyright (c) 2018, Julian Thome
#
# Released under the MIT license
# https://opensource.org/licenses/MIT


MAIN="$1"
BNAME="baseline-diff"

[ -z "$MAIN" ] && {
    echo "usage: $0 <main-file>"
    echo "<main-file>: the name name of the latex main file (including .tex extension)"
    exit 1
}

OLD="$(git rev-parse --short $BNAME)"
NEW="$(git rev-parse --short HEAD)"
#GITREPO="$(git remote get-url origin)"

# assume that this script is executed with inside the latex repo
GITREPO="$(pwd)"

[ -z "$OLD" ] && [ -f ".$BNAME" ] && OLD="$(cat .$BNAME)"

[ -z "$OLD" ] && {
	echo "neither a valid baseline file .$BNAME nor a valid git tag $BNAME were provided"
	exit 0
}

$(dirname "${BASH_SOURCE[0]}")/do_latexdiff.sh "$GITREPO" "$OLD" "$NEW" "$MAIN"

# file does not exist
[ -e "/tmp/diff.pdf" ] || exit 1

# giving access to the artifact
cp /tmp/diff.pdf .

exit 0
